using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

public class chat : MonoBehaviour
{
    public GameObject uiObject; // Reference to the UI object to activate
    public XRBaseInteractable interactable; // Reference to the XR Interactable component

    private void Start()
    {
        // Get a reference to the XR Interactable component attached to this object
        interactable = GetComponent<XRBaseInteractable>();

        // Subscribe to the XR Interactable's onSelectEntered event
        interactable.onSelectEntered.AddListener(OnSelectEntered);
    }

    private void OnSelectEntered(XRBaseInteractor interactor)
    {
        // Activate the UI object
        uiObject.SetActive(true);
    }
}