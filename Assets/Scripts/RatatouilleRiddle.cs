using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RatatouilleRiddle : MonoBehaviour
{
    public GameObject mysteryObject;

    private bool hasTomato;
    private bool hasEggplant;
    private bool hasZucchini;

    // Start is called before the first frame update
    void Start()
    {
        hasTomato = false;
        hasEggplant = false;
        hasZucchini = false;
}

    // Update is called once per frame
    void Update()
    {
        if(hasTomato && hasEggplant && hasZucchini)
        {
            mysteryObject.GetComponent<MeshRenderer>().enabled = true;
            mysteryObject.GetComponent<Rigidbody>().isKinematic = false;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "tomato")
        {
            hasTomato = true;
            other.gameObject.GetComponent<MeshRenderer>().enabled = false;
        }
        else if(other.tag == "eggplant")
        {
            hasEggplant = true;
            other.gameObject.GetComponent<MeshRenderer>().enabled = false;
        }
        else if(other.tag == "zucchini")
        {
            hasZucchini = true;
            other.gameObject.GetComponent<MeshRenderer>().enabled = false;
        }
    }
}
