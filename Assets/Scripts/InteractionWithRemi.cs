using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractionWithRemi : MonoBehaviour
{
    public GameObject remi;
    public GameObject chat;
    public GameObject hint;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (this.transform.Find("Camera Offset").Find("Main Camera"))
        {
            Vector3 CameraPos = this.transform.Find("Camera Offset").Find("Main Camera").transform.position;
            if (Vector3.Distance(CameraPos, remi.transform.position) <= 2 && Vector3.Distance(CameraPos, remi.transform.position) >= 1)
            {
                hint.SetActive(true);
            }
            else
            {
                hint.SetActive(false);
            }
        }

        if (this.transform.Find("Camera Offset").Find("Main Camera"))
        {
            Vector3 CameraPos = this.transform.Find("Camera Offset").Find("Main Camera").transform.position;
            if(Vector3.Distance(CameraPos, remi.transform.position) <= 1)
            {
                chat.SetActive(true);
            }
            else
            {
                chat.SetActive(false);
            }
        }


    }
}
