using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.SceneManagement;

public class Projector : MonoBehaviour
{
    public GameObject screen;
    public GameObject lightBleam;
    private Animator animator;
    private VideoPlayer videoPlayer;

    // Start is called before the first frame update
    void Start()
    {
        animator = gameObject.GetComponent<Animator>();
        videoPlayer = screen.GetComponent<VideoPlayer>();
    }

    void PlayProjector(string movie)
    {
        if (movie == "ratatouille")
        {
            videoPlayer.loopPointReached += LoadRatatouille;
        }
        animator.SetBool("isRunning", true);
        videoPlayer.Play();
    }

    void LoadRatatouille(VideoPlayer vp)
    {
        SceneManager.LoadScene("RatatouilleScene");
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "ratatouille")
        {
            PlayProjector("ratatouille");
            other.GetComponent<Rigidbody>().useGravity = false;
            other.GetComponent<MeshRenderer>().enabled = false;
            lightBleam.GetComponent<MeshRenderer>().enabled = true;
        }
    }
}
