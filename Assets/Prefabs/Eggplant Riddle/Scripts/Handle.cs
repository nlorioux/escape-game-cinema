using System.Collections;
using System.Collections.Generic;
using UnityEditor.Experimental.GraphView;
using UnityEngine;

public class Handle : MonoBehaviour
{
    public GameObject hand;
    public GameObject locker;
    public bool isTouchingDrawer;
    public bool spaceDown;
    public bool objectCaught;
    private Vector3 direction = new Vector3(1, 0, 0);
    // Start is called before the first frame update
    void Start()
    {
        objectCaught= false;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            spaceDown= true;
        }
        if (Input.GetKeyUp(KeyCode.Space))
        {
            spaceDown = false;
        }


        if (isTouchingDrawer && spaceDown && !objectCaught)
        {
            if(!locker.gameObject.GetComponent<OpenLocker>().isLocked)
            {
                this.gameObject.transform.SetPositionAndRotation(transform.position, transform.rotation);
                this.gameObject.transform.SetParent(hand.transform, true);
                objectCaught = true;
            }
            
        }

        if (!spaceDown)
        {
            this.gameObject.transform.SetParent(null, true);
            objectCaught= false;
        }
        /*
        if (isTouchingDrawer && spaceDown)
        {
            this.gameObject.transform.parent.SetParent(hand.transform, true);
        }
        Debug.Log(transform.TransformPoint(hand.transform.position).z);
        
        if (!spaceDown)
        {
            this.gameObject.transform.parent.SetParent(null, true);
        }
        */


        /*
        if (!spaceDown || transform.TransformPoint(hand.transform.position).y > 2.8f || transform.TransformPoint(hand.transform.position).y <2.7f ||transform.TransformPoint(transform.position).z > 0.35f || transform.TransformPoint(transform.position).y < 0.33f)
        {
            
            this.gameObject.transform.parent.SetParent(null, true);
        }
        */
    }
    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject == hand)
        {  
            isTouchingDrawer = true;
            
        }
    }
    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject == hand)
        {
            isTouchingDrawer = false;
        }
    }
}
