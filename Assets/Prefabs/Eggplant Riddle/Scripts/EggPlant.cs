using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static UnityEngine.ParticleSystem;
using UnityEngine.SocialPlatforms.Impl;

public class EggPlant : MonoBehaviour
{
    public bool isInTheDrawer=false;
    public GameObject Drawer;
    // Start is called before the first frame update
    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Drawer")
        {
            isInTheDrawer = true;
            this.gameObject.transform.SetParent(Drawer.transform);

        }
        //if (collision.gameObject.tag == "Hand")
        //{
        //    isInTheDrawer = false;
        //    this.gameObject.transform.SetParent(null);

        //}
    }
    private void OnCollisionExit(Collision other)
    {
        if (other.gameObject.tag == "Drawer")
        {
            isInTheDrawer = false;
            this.gameObject.transform.SetParent(null);
        }
    }
}
